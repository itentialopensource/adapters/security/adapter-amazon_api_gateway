/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-amazon_api_gateway',
      type: 'AmazonApiGateway',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AmazonApiGateway = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Amazon_api_gateway Adapter Test', () => {
  describe('AmazonApiGateway Class Tests', () => {
    const a = new AmazonApiGateway(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const apisCreateApiBodyParam = {
      name: 'string',
      protocolType: 'WEBSOCKET',
      routeSelectionExpression: 'string'
    };
    describe('#createApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApi(apisCreateApiBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiEndpoint);
                assert.equal('string', data.response.ApiId);
                assert.equal('string', data.response.ApiKeySelectionExpression);
                assert.equal('string', data.response.CreatedDate);
                assert.equal('string', data.response.Description);
                assert.equal(false, data.response.DisableSchemaValidation);
                assert.equal('string', data.response.Name);
                assert.equal('WEBSOCKET', data.response.ProtocolType);
                assert.equal('string', data.response.RouteSelectionExpression);
                assert.equal('object', typeof data.response.Tags);
                assert.equal('string', data.response.Version);
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisApiId = 'fakedata';
    const apisCreateAuthorizerBodyParam = {
      authorizerType: 'REQUEST',
      authorizerUri: 'string',
      identitySource: [
        'string'
      ],
      name: 'string'
    };
    describe('#createAuthorizer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAuthorizer(apisApiId, apisCreateAuthorizerBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.AuthorizerCredentialsArn);
                assert.equal('string', data.response.AuthorizerId);
                assert.equal(10, data.response.AuthorizerResultTtlInSeconds);
                assert.equal('REQUEST', data.response.AuthorizerType);
                assert.equal('string', data.response.AuthorizerUri);
                assert.equal(true, Array.isArray(data.response.IdentitySource));
                assert.equal('string', data.response.IdentityValidationExpression);
                assert.equal('string', data.response.Name);
                assert.equal(true, Array.isArray(data.response.ProviderArns));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createAuthorizer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisCreateDeploymentBodyParam = {
      description: 'string',
      stageName: 'string'
    };
    describe('#createDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeployment(apisApiId, apisCreateDeploymentBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.CreatedDate);
                assert.equal('string', data.response.DeploymentId);
                assert.equal('FAILED', data.response.DeploymentStatus);
                assert.equal('string', data.response.DeploymentStatusMessage);
                assert.equal('string', data.response.Description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisCreateIntegrationBodyParam = {
      integrationType: 'HTTP_PROXY'
    };
    describe('#createIntegration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIntegration(apisApiId, apisCreateIntegrationBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ConnectionId);
                assert.equal('INTERNET', data.response.ConnectionType);
                assert.equal('CONVERT_TO_BINARY', data.response.ContentHandlingStrategy);
                assert.equal('string', data.response.CredentialsArn);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.IntegrationId);
                assert.equal('string', data.response.IntegrationMethod);
                assert.equal('string', data.response.IntegrationResponseSelectionExpression);
                assert.equal('AWS_PROXY', data.response.IntegrationType);
                assert.equal('string', data.response.IntegrationUri);
                assert.equal('WHEN_NO_TEMPLATES', data.response.PassthroughBehavior);
                assert.equal('object', typeof data.response.RequestParameters);
                assert.equal('object', typeof data.response.RequestTemplates);
                assert.equal('string', data.response.TemplateSelectionExpression);
                assert.equal(2, data.response.TimeoutInMillis);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisIntegrationId = 'fakedata';
    const apisCreateIntegrationResponseBodyParam = {
      integrationResponseKey: 'string'
    };
    describe('#createIntegrationResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIntegrationResponse(apisApiId, apisIntegrationId, apisCreateIntegrationResponseBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('CONVERT_TO_BINARY', data.response.ContentHandlingStrategy);
                assert.equal('string', data.response.IntegrationResponseId);
                assert.equal('string', data.response.IntegrationResponseKey);
                assert.equal('object', typeof data.response.ResponseParameters);
                assert.equal('object', typeof data.response.ResponseTemplates);
                assert.equal('string', data.response.TemplateSelectionExpression);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createIntegrationResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisCreateModelBodyParam = {
      name: 'string',
      schema: 'string'
    };
    describe('#createModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createModel(apisApiId, apisCreateModelBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ContentType);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.ModelId);
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Schema);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisCreateRouteBodyParam = {
      routeKey: 'string'
    };
    describe('#createRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRoute(apisApiId, apisCreateRouteBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.ApiKeyRequired);
                assert.equal(true, Array.isArray(data.response.AuthorizationScopes));
                assert.equal('NONE', data.response.AuthorizationType);
                assert.equal('string', data.response.AuthorizerId);
                assert.equal('string', data.response.ModelSelectionExpression);
                assert.equal('string', data.response.OperationName);
                assert.equal('object', typeof data.response.RequestModels);
                assert.equal('object', typeof data.response.RequestParameters);
                assert.equal('string', data.response.RouteId);
                assert.equal('string', data.response.RouteKey);
                assert.equal('string', data.response.RouteResponseSelectionExpression);
                assert.equal('string', data.response.Target);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisRouteId = 'fakedata';
    const apisCreateRouteResponseBodyParam = {
      routeResponseKey: 'string'
    };
    describe('#createRouteResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRouteResponse(apisApiId, apisRouteId, apisCreateRouteResponseBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ModelSelectionExpression);
                assert.equal('object', typeof data.response.ResponseModels);
                assert.equal('object', typeof data.response.ResponseParameters);
                assert.equal('string', data.response.RouteResponseId);
                assert.equal('string', data.response.RouteResponseKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createRouteResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisCreateStageBodyParam = {
      stageName: 'string'
    };
    describe('#createStage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStage(apisApiId, apisCreateStageBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.AccessLogSettings);
                assert.equal('string', data.response.ClientCertificateId);
                assert.equal('string', data.response.CreatedDate);
                assert.equal('object', typeof data.response.DefaultRouteSettings);
                assert.equal('string', data.response.DeploymentId);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.LastUpdatedDate);
                assert.equal('object', typeof data.response.RouteSettings);
                assert.equal('string', data.response.StageName);
                assert.equal('object', typeof data.response.StageVariables);
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'createStage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApis(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getApis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisUpdateApiBodyParam = {
      apiKeySelectionExpression: 'string',
      description: 'string',
      disableSchemaValidation: false,
      name: 'string',
      routeSelectionExpression: 'string',
      version: 'string'
    };
    describe('#updateApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApi(apisApiId, apisUpdateApiBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiEndpoint);
                assert.equal('string', data.response.ApiId);
                assert.equal('string', data.response.ApiKeySelectionExpression);
                assert.equal('string', data.response.CreatedDate);
                assert.equal('string', data.response.Description);
                assert.equal(false, data.response.DisableSchemaValidation);
                assert.equal('string', data.response.Name);
                assert.equal('WEBSOCKET', data.response.ProtocolType);
                assert.equal('string', data.response.RouteSelectionExpression);
                assert.equal('object', typeof data.response.Tags);
                assert.equal('string', data.response.Version);
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApi(apisApiId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiEndpoint);
                assert.equal('string', data.response.ApiId);
                assert.equal('string', data.response.ApiKeySelectionExpression);
                assert.equal('string', data.response.CreatedDate);
                assert.equal('string', data.response.Description);
                assert.equal(false, data.response.DisableSchemaValidation);
                assert.equal('string', data.response.Name);
                assert.equal('WEBSOCKET', data.response.ProtocolType);
                assert.equal('string', data.response.RouteSelectionExpression);
                assert.equal('object', typeof data.response.Tags);
                assert.equal('string', data.response.Version);
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthorizers(apisApiId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getAuthorizers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisAuthorizerId = 'fakedata';
    const apisUpdateAuthorizerBodyParam = {
      authorizerCredentialsArn: 'string',
      authorizerResultTtlInSeconds: 5,
      authorizerType: 'REQUEST',
      authorizerUri: 'string',
      identitySource: [
        'string'
      ],
      identityValidationExpression: 'string',
      name: 'string',
      providerArns: [
        'string'
      ]
    };
    describe('#updateAuthorizer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAuthorizer(apisApiId, apisAuthorizerId, apisUpdateAuthorizerBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.AuthorizerCredentialsArn);
                assert.equal('string', data.response.AuthorizerId);
                assert.equal(7, data.response.AuthorizerResultTtlInSeconds);
                assert.equal('REQUEST', data.response.AuthorizerType);
                assert.equal('string', data.response.AuthorizerUri);
                assert.equal(true, Array.isArray(data.response.IdentitySource));
                assert.equal('string', data.response.IdentityValidationExpression);
                assert.equal('string', data.response.Name);
                assert.equal(true, Array.isArray(data.response.ProviderArns));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateAuthorizer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthorizer(apisApiId, apisAuthorizerId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.AuthorizerCredentialsArn);
                assert.equal('string', data.response.AuthorizerId);
                assert.equal(4, data.response.AuthorizerResultTtlInSeconds);
                assert.equal('REQUEST', data.response.AuthorizerType);
                assert.equal('string', data.response.AuthorizerUri);
                assert.equal(true, Array.isArray(data.response.IdentitySource));
                assert.equal('string', data.response.IdentityValidationExpression);
                assert.equal('string', data.response.Name);
                assert.equal(true, Array.isArray(data.response.ProviderArns));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getAuthorizer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeployments(apisApiId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getDeployments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisDeploymentId = 'fakedata';
    const apisUpdateDeploymentBodyParam = {
      description: 'string'
    };
    describe('#updateDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeployment(apisApiId, apisDeploymentId, apisUpdateDeploymentBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.CreatedDate);
                assert.equal('string', data.response.DeploymentId);
                assert.equal('FAILED', data.response.DeploymentStatus);
                assert.equal('string', data.response.DeploymentStatusMessage);
                assert.equal('string', data.response.Description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeployment(apisApiId, apisDeploymentId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.CreatedDate);
                assert.equal('string', data.response.DeploymentId);
                assert.equal('PENDING', data.response.DeploymentStatus);
                assert.equal('string', data.response.DeploymentStatusMessage);
                assert.equal('string', data.response.Description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegrations(apisApiId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getIntegrations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisUpdateIntegrationBodyParam = {
      connectionId: 'string',
      connectionType: 'VPC_LINK',
      contentHandlingStrategy: 'CONVERT_TO_TEXT',
      credentialsArn: 'string',
      description: 'string',
      integrationMethod: 'string',
      integrationType: 'AWS_PROXY',
      integrationUri: 'string',
      passthroughBehavior: 'NEVER',
      requestParameters: {},
      requestTemplates: {},
      templateSelectionExpression: 'string',
      timeoutInMillis: 5
    };
    describe('#updateIntegration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIntegration(apisApiId, apisIntegrationId, apisUpdateIntegrationBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ConnectionId);
                assert.equal('INTERNET', data.response.ConnectionType);
                assert.equal('CONVERT_TO_BINARY', data.response.ContentHandlingStrategy);
                assert.equal('string', data.response.CredentialsArn);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.IntegrationId);
                assert.equal('string', data.response.IntegrationMethod);
                assert.equal('string', data.response.IntegrationResponseSelectionExpression);
                assert.equal('AWS', data.response.IntegrationType);
                assert.equal('string', data.response.IntegrationUri);
                assert.equal('NEVER', data.response.PassthroughBehavior);
                assert.equal('object', typeof data.response.RequestParameters);
                assert.equal('object', typeof data.response.RequestTemplates);
                assert.equal('string', data.response.TemplateSelectionExpression);
                assert.equal(5, data.response.TimeoutInMillis);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegration(apisApiId, apisIntegrationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ConnectionId);
                assert.equal('INTERNET', data.response.ConnectionType);
                assert.equal('CONVERT_TO_TEXT', data.response.ContentHandlingStrategy);
                assert.equal('string', data.response.CredentialsArn);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.IntegrationId);
                assert.equal('string', data.response.IntegrationMethod);
                assert.equal('string', data.response.IntegrationResponseSelectionExpression);
                assert.equal('MOCK', data.response.IntegrationType);
                assert.equal('string', data.response.IntegrationUri);
                assert.equal('WHEN_NO_TEMPLATES', data.response.PassthroughBehavior);
                assert.equal('object', typeof data.response.RequestParameters);
                assert.equal('object', typeof data.response.RequestTemplates);
                assert.equal('string', data.response.TemplateSelectionExpression);
                assert.equal(6, data.response.TimeoutInMillis);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrationResponses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegrationResponses(apisApiId, apisIntegrationId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getIntegrationResponses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisIntegrationResponseId = 'fakedata';
    const apisUpdateIntegrationResponseBodyParam = {
      contentHandlingStrategy: 'CONVERT_TO_TEXT',
      integrationResponseKey: 'string',
      responseParameters: {},
      responseTemplates: {},
      templateSelectionExpression: 'string'
    };
    describe('#updateIntegrationResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIntegrationResponse(apisApiId, apisIntegrationId, apisIntegrationResponseId, apisUpdateIntegrationResponseBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('CONVERT_TO_BINARY', data.response.ContentHandlingStrategy);
                assert.equal('string', data.response.IntegrationResponseId);
                assert.equal('string', data.response.IntegrationResponseKey);
                assert.equal('object', typeof data.response.ResponseParameters);
                assert.equal('object', typeof data.response.ResponseTemplates);
                assert.equal('string', data.response.TemplateSelectionExpression);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateIntegrationResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntegrationResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntegrationResponse(apisApiId, apisIntegrationId, apisIntegrationResponseId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('CONVERT_TO_BINARY', data.response.ContentHandlingStrategy);
                assert.equal('string', data.response.IntegrationResponseId);
                assert.equal('string', data.response.IntegrationResponseKey);
                assert.equal('object', typeof data.response.ResponseParameters);
                assert.equal('object', typeof data.response.ResponseTemplates);
                assert.equal('string', data.response.TemplateSelectionExpression);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getIntegrationResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getModels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getModels(apisApiId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getModels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisModelId = 'fakedata';
    const apisUpdateModelBodyParam = {
      contentType: 'string',
      description: 'string',
      name: 'string',
      schema: 'string'
    };
    describe('#updateModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateModel(apisApiId, apisModelId, apisUpdateModelBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ContentType);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.ModelId);
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Schema);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getModel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getModel(apisApiId, apisModelId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ContentType);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.ModelId);
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Schema);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getModelTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getModelTemplate(apisApiId, apisModelId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.Value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getModelTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoutes(apisApiId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisUpdateRouteBodyParam = {
      apiKeyRequired: false,
      authorizationScopes: [
        'string'
      ],
      authorizationType: 'NONE',
      authorizerId: 'string',
      modelSelectionExpression: 'string',
      operationName: 'string',
      requestModels: {},
      requestParameters: {},
      routeKey: 'string',
      routeResponseSelectionExpression: 'string',
      target: 'string'
    };
    describe('#updateRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRoute(apisApiId, apisRouteId, apisUpdateRouteBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.ApiKeyRequired);
                assert.equal(true, Array.isArray(data.response.AuthorizationScopes));
                assert.equal('AWS_IAM', data.response.AuthorizationType);
                assert.equal('string', data.response.AuthorizerId);
                assert.equal('string', data.response.ModelSelectionExpression);
                assert.equal('string', data.response.OperationName);
                assert.equal('object', typeof data.response.RequestModels);
                assert.equal('object', typeof data.response.RequestParameters);
                assert.equal('string', data.response.RouteId);
                assert.equal('string', data.response.RouteKey);
                assert.equal('string', data.response.RouteResponseSelectionExpression);
                assert.equal('string', data.response.Target);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoute(apisApiId, apisRouteId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.ApiKeyRequired);
                assert.equal(true, Array.isArray(data.response.AuthorizationScopes));
                assert.equal('AWS_IAM', data.response.AuthorizationType);
                assert.equal('string', data.response.AuthorizerId);
                assert.equal('string', data.response.ModelSelectionExpression);
                assert.equal('string', data.response.OperationName);
                assert.equal('object', typeof data.response.RequestModels);
                assert.equal('object', typeof data.response.RequestParameters);
                assert.equal('string', data.response.RouteId);
                assert.equal('string', data.response.RouteKey);
                assert.equal('string', data.response.RouteResponseSelectionExpression);
                assert.equal('string', data.response.Target);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteResponses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRouteResponses(apisApiId, null, null, apisRouteId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getRouteResponses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisRouteResponseId = 'fakedata';
    const apisUpdateRouteResponseBodyParam = {
      modelSelectionExpression: 'string',
      responseModels: {},
      responseParameters: {},
      routeResponseKey: 'string'
    };
    describe('#updateRouteResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRouteResponse(apisApiId, apisRouteId, apisRouteResponseId, apisUpdateRouteResponseBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ModelSelectionExpression);
                assert.equal('object', typeof data.response.ResponseModels);
                assert.equal('object', typeof data.response.ResponseParameters);
                assert.equal('string', data.response.RouteResponseId);
                assert.equal('string', data.response.RouteResponseKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateRouteResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRouteResponse(apisApiId, apisRouteId, apisRouteResponseId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ModelSelectionExpression);
                assert.equal('object', typeof data.response.ResponseModels);
                assert.equal('object', typeof data.response.ResponseParameters);
                assert.equal('string', data.response.RouteResponseId);
                assert.equal('string', data.response.RouteResponseKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getRouteResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStages(apisApiId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getStages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apisStageName = 'fakedata';
    const apisUpdateStageBodyParam = {
      accessLogSettings: {
        DestinationArn: 'string',
        Format: 'string'
      },
      clientCertificateId: 'string',
      defaultRouteSettings: {
        DataTraceEnabled: true,
        DetailedMetricsEnabled: false,
        LoggingLevel: 'INFO',
        ThrottlingBurstLimit: 6,
        ThrottlingRateLimit: 1
      },
      deploymentId: 'string',
      description: 'string',
      routeSettings: {},
      stageVariables: {}
    };
    describe('#updateStage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStage(apisApiId, apisStageName, apisUpdateStageBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.AccessLogSettings);
                assert.equal('string', data.response.ClientCertificateId);
                assert.equal('string', data.response.CreatedDate);
                assert.equal('object', typeof data.response.DefaultRouteSettings);
                assert.equal('string', data.response.DeploymentId);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.LastUpdatedDate);
                assert.equal('object', typeof data.response.RouteSettings);
                assert.equal('string', data.response.StageName);
                assert.equal('object', typeof data.response.StageVariables);
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'updateStage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStage(apisApiId, apisStageName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.AccessLogSettings);
                assert.equal('string', data.response.ClientCertificateId);
                assert.equal('string', data.response.CreatedDate);
                assert.equal('object', typeof data.response.DefaultRouteSettings);
                assert.equal('string', data.response.DeploymentId);
                assert.equal('string', data.response.Description);
                assert.equal('string', data.response.LastUpdatedDate);
                assert.equal('object', typeof data.response.RouteSettings);
                assert.equal('string', data.response.StageName);
                assert.equal('object', typeof data.response.StageVariables);
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'getStage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const domainnamesCreateDomainNameBodyParam = {
      domainName: 'string'
    };
    describe('#createDomainName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDomainName(domainnamesCreateDomainNameBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiMappingSelectionExpression);
                assert.equal('string', data.response.DomainName);
                assert.equal(true, Array.isArray(data.response.DomainNameConfigurations));
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'createDomainName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const domainnamesDomainName = 'fakedata';
    const domainnamesCreateApiMappingBodyParam = {
      apiId: 'string',
      stage: 'string'
    };
    describe('#createApiMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApiMapping(domainnamesDomainName, domainnamesCreateApiMappingBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiId);
                assert.equal('string', data.response.ApiMappingId);
                assert.equal('string', data.response.ApiMappingKey);
                assert.equal('string', data.response.Stage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'createApiMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNames - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNames(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'getDomainNames', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const domainnamesUpdateDomainNameBodyParam = {
      domainNameConfigurations: [
        {
          ApiGatewayDomainName: 'string',
          CertificateArn: 'string',
          CertificateName: 'string',
          CertificateUploadDate: 'string',
          DomainNameStatus: 'UPDATING',
          DomainNameStatusMessage: 'string',
          EndpointType: 'REGIONAL',
          HostedZoneId: 'string',
          SecurityPolicy: 'TLS_1_2'
        }
      ]
    };
    describe('#updateDomainName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDomainName(domainnamesDomainName, domainnamesUpdateDomainNameBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiMappingSelectionExpression);
                assert.equal('string', data.response.DomainName);
                assert.equal(true, Array.isArray(data.response.DomainNameConfigurations));
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'updateDomainName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainName(domainnamesDomainName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiMappingSelectionExpression);
                assert.equal('string', data.response.DomainName);
                assert.equal(true, Array.isArray(data.response.DomainNameConfigurations));
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'getDomainName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiMappings(domainnamesDomainName, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Items));
                assert.equal('string', data.response.NextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'getApiMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const domainnamesApiMappingId = 'fakedata';
    const domainnamesUpdateApiMappingBodyParam = {
      apiId: 'string'
    };
    describe('#updateApiMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApiMapping(domainnamesApiMappingId, domainnamesDomainName, domainnamesUpdateApiMappingBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiId);
                assert.equal('string', data.response.ApiMappingId);
                assert.equal('string', data.response.ApiMappingKey);
                assert.equal('string', data.response.Stage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'updateApiMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiMapping(domainnamesApiMappingId, domainnamesDomainName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ApiId);
                assert.equal('string', data.response.ApiMappingId);
                assert.equal('string', data.response.ApiMappingKey);
                assert.equal('string', data.response.Stage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'getApiMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsResourceArn = 'fakedata';
    const tagsTagResourceBodyParam = {
      tags: {}
    };
    describe('#tagResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tagResource(tagsResourceArn, tagsTagResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'tagResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTags(tagsResourceArn, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.Tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'getTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApi - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApi(apisApiId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthorizer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthorizer(apisApiId, apisAuthorizerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteAuthorizer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeployment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeployment(apisApiId, apisDeploymentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIntegration(apisApiId, apisIntegrationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteIntegration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegrationResponse - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIntegrationResponse(apisApiId, apisIntegrationId, apisIntegrationResponseId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteIntegrationResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteModel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteModel(apisApiId, apisModelId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteModel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoute(apisApiId, apisRouteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteResponse - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRouteResponse(apisApiId, apisRouteId, apisRouteResponseId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteRouteResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteStage(apisApiId, apisStageName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Apis', 'deleteStage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDomainName(domainnamesDomainName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'deleteDomainName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiMapping(domainnamesApiMappingId, domainnamesDomainName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domainnames', 'deleteApiMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsTagKeys = [];
    describe('#untagResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.untagResource(tagsResourceArn, tagsTagKeys, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_api_gateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'untagResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
