# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Amazon_api_gateway System. The API that was used to build the adapter for Amazon_api_gateway is usually available in the report directory of this adapter. The adapter utilizes the Amazon_api_gateway API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS API Gateway adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS API Gateway to that enable developers to create, publish, maintain, monitor, and secure APIs at any scale.

With this adapter you have the ability to perform operations with AWS API Gateway such as:

- Get Domain Names
- Create Domain Name
- Get Route
- Update Route
- Get Deployment
- Tag Resource

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
